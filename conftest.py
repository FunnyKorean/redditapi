import os
from multiprocessing import Process

import pytest

from files import get_file_name
from post_model import Post
from server import run

filename = get_file_name()


@pytest.fixture(scope='function')
def server(data_filepath):
    server = Process(target=run)
    server.start()
    yield server
    # After tests are done, terminate the server process.
    server.terminate()
    server.join()


@pytest.fixture
def data_filepath():
    data_filepath = get_file_name()
    yield data_filepath
    os.remove(f'records/{filename}.txt')


@pytest.fixture
def valid_post():
    return Post(
        **{
            "UNIQUE_ID": "3278c49f7fad11eebf07f46add75dd512",
            "link": "/r/MadeMeSmile/comments/17hu67l/gratefulness/",
            "username": "c4gtay",
            "user_cake_data": "Dec 23, 2022",
            "user_post_karma": 31155,
            "user_comment_karma": 4676,
            "post_date": "2023-10-27T18:32:02.208000+0000",
            "number_of_comments": 3522,
            "number_of_votes": 131836,
            "post_category": "Wholesale moments"
        }
    )


@pytest.fixture
def updated_post():
    return Post(
        **{
            "UNIQUE_ID": "3278c49f7fad11eebf07f46add75dd512",
            "link": "/r/MadeMeSmile/comments/17hu67l/gratefulness/",
            "username": "c4gtay",
            "user_cake_data": "Dec 23, 2022",
            "user_post_karma": 31155,
            "user_comment_karma": 4676,
            "post_date": "2023-10-27T18:32:02.208000+0000",
            "number_of_comments": 3522,
            "number_of_votes": 131836,
            "post_category": "Comedy Club"
        }
    )


@pytest.fixture
def post_does_not_exist():
    return Post(
        **{
            "UNIQUE_ID": "512",
            "link": "/r/MadeMeSmile/comments/17hu67l/gratefulness/",
            "username": "c4gtay",
            "user_cake_data": "Dec 23, 2022",
            "user_post_karma": 31155,
            "user_comment_karma": 4676,
            "post_date": "2023-10-27T18:32:02.208000+0000",
            "number_of_comments": 3522,
            "number_of_votes": 131836,
            "post_category": "Comedy Club"
        }
    )
