from datetime import datetime

from post_model import Post
import os


def check_post_exists(filename: str, post: Post) -> tuple[bool, int]:
    records = read_lines(filename)
    record = f'{post.get_data()}\n'
    count = 0
    for data in records:
        count += 1
        if record == data:
            return True, count
    return False, count + 1


def write_to_file(filename: str, post: Post) -> tuple:
    record_exists, line = check_post_exists(filename, post)
    if not record_exists:
        with open(f'records/{filename}.txt', 'a+', encoding='utf-8') as file:
            file.write(f'{post.get_data()}\n')
    return line, record_exists


def read_lines(filename: str):
    with open(f'records/{filename}.txt', 'r') as file:
        lines = file.readlines()
        return lines


def get_file_name() -> str:
    current_time = datetime.now().strftime('%Y%m%d')
    file_name = f'reddit-{current_time}'
    if not os.path.exists(f'records/{file_name}.txt'):
        open(f'records/{file_name}.txt', 'w').close()
    return file_name


def convert_line_to_dict(data: str) -> dict:
    values = data.replace('\n', '').split(';')

    data = {'UNIQUE_ID': values[0], 'link': values[1],
            'username': values[2], 'user_cake_data': values[3],
            'user_post_karma': values[4], 'user_comment_karma': values[5],
            'post_date': values[6], 'number_of_comments': values[7],
            'number_of_votes': values[8], 'post_category': values[9]}

    return data


def get_records(filename):
    lines = read_lines(filename)
    records = list(map(convert_line_to_dict, lines))
    return records


def get_record(filename: str, post_id: str):
    records = get_records(filename)
    for record in records:
        if record['UNIQUE_ID'] == post_id:
            return record
    return None


def delete_record(filename: str, post_id: str) -> bool:
    lines = read_lines(filename)
    is_found = False
    with open(f'records/{filename}.txt', 'w', encoding='utf-8') as file:
        for line in lines:
            id_ = line.split(';')[0]
            if post_id == id_:
                is_found = True
                continue
            file.write(line)
        return is_found


def update_record(filename: str, post: Post):
    lines = read_lines(filename)
    is_found = False
    with open(f'records/{filename}.txt', 'w', encoding='utf-8') as file:
        for line in lines:
            id_ = line.split(';')[0]
            if post.UNIQUE_ID == id_:
                is_found = True
                data = f'{post.get_data()}\n'
                file.write(data)
                continue
            file.write(line)
        return is_found
