import json
from http.server import BaseHTTPRequestHandler, HTTPServer

from files import get_file_name
from routes import ROUTES, Method, Response
from url_parser import parse_request_url

GET_ROUTES = ROUTES[Method.GET]
POST_ROUTES = ROUTES[Method.POST]
PUT_ROUTES = ROUTES[Method.PUT]
DELETE_ROUTES = ROUTES[Method.DELETE]


class HttpRequestHandler(BaseHTTPRequestHandler):
    FILE_NAME = None
    resp = Response({'message': 'route not found'}, 404)

    def __init__(self, request, client_address, server):
        super().__init__(request, client_address, server)
        if not self.FILE_NAME:
            self.FILE_NAME = get_file_name()

    def _set_headers(self, content_type="application/json", status_code=200):
        self.send_response(status_code)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        body = {}
        data = parse_request_url(self.path)
        func = GET_ROUTES.get(data['path'])
        if data['type_error']:
            self.resp = Response({'message': 'argument type error'}, 404)
        elif func:
            self.resp = func(body, **data['arguments'])

        self._set_headers(status_code=self.resp.status)
        response = json.dumps(self.resp.message).encode('utf-8')
        self.wfile.write(response)

    def do_POST(self):

        content_len = int(self.headers.get('content-length', 0))
        payload = self.rfile.read(content_len)
        body = json.loads(payload)

        data = parse_request_url(self.path)
        func = POST_ROUTES.get(data['path'])
        if data['type_error']:
            self.resp = Response({'message': 'argument type error'}, 404)
        elif func:
            self.resp = func(body, **data['arguments'])

        self._set_headers(status_code=self.resp.status)
        response = json.dumps(self.resp.message).encode('utf-8')
        self.wfile.write(response)

    def do_DELETE(self):
        body = {}
        data = parse_request_url(self.path)
        func = DELETE_ROUTES.get(data['path'])
        if data['type_error']:
            self.resp = Response({'message': 'argument type error'}, 404)
        elif func:
            self.resp = func(body, **data['arguments'])

        self._set_headers(status_code=self.resp.status)
        response = json.dumps(self.resp.message).encode('utf-8')
        self.wfile.write(response)

    def do_PUT(self):
        content_len = int(self.headers.get('content-length', 0))
        payload = self.rfile.read(content_len)
        body = json.loads(payload)

        data = parse_request_url(self.path)
        func = PUT_ROUTES.get(data['path'])
        if data['type_error']:
            self.resp = Response({'message': 'argument type error'}, 404)
        elif func:
            self.resp = func(body, **data['arguments'])

        self._set_headers(status_code=self.resp.status)
        response = json.dumps(self.resp.message).encode('utf-8')
        self.wfile.write(response)


def run(server_class=HTTPServer, handler_class=HttpRequestHandler):
    server_address = ('127.0.0.1', 8087)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()


if __name__ == '__main__':
    run()
