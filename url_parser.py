from routes import templates


def parse_request_url(path: str) -> dict:
    path_message = {}
    for template in templates:
        path_message['arguments'] = {}
        path_message['type_error'] = False
        matches = 0
        path_lst = split_path(path)
        template_lst = split_path(template)
        if len(path_lst) != len(template_lst):
            continue
        for index, temp_item in enumerate(template_lst):
            if temp_item == path_lst[index]:
                matches += 1
            elif temp_item.startswith('<') and temp_item.endswith('>'):
                temp_item = temp_item.replace('<', '').replace('>', '')
                lst = temp_item.split(':')
                if lst[0] == 'int':
                    path_message['type_error'] = not path_lst[index].isdigit()
                    if not path_message['type_error']:
                        path_message['arguments'][lst[1]] = int(path_lst[index])
                    matches += 1
                else:
                    path_message['arguments'][lst[1]] = path_lst[index]
                    matches += 1

            if matches == len(template_lst):
                path_message['path'] = template
                return path_message

        path_message['path'] = 'invalid-path'

    return path_message


def split_path(path: str) -> list[str]:
    return list(filter(None, path.split('/')))

