import json

import requests

from files import write_to_file, check_post_exists

expected_response = {'UNIQUE_ID': '3278c49f7fad11eebf07f46add75dd512',
                     'link': '/r/MadeMeSmile/comments/17hu67l/gratefulness/', 'username': 'c4gtay',
                     'user_cake_data': 'Dec 23, 2022', 'user_post_karma': '31155', 'user_comment_karma': '4676',
                     'post_date': '2023-10-27T18:32:02.208000+0000', 'number_of_comments': '3522',
                     'number_of_votes': '131836', 'post_category': 'Wholesale moments'}


def test_get_posts_empty(server):
    response = requests.get('http://localhost:8087/posts')
    assert response.status_code == 200
    assert response.json() == {'message': []}


def test_get_posts(server, valid_post, data_filepath):
    write_to_file(data_filepath, valid_post)
    response = requests.get('http://localhost:8087/posts')
    assert response.status_code == 200
    assert response.json() == {'message': [expected_response]}


def test_get_post(server, valid_post, data_filepath):
    write_to_file(data_filepath, valid_post)
    response = requests.get('http://localhost:8087/posts/3278c49f7fad11eebf07f46add75dd512')
    assert response.status_code == 200
    assert response.json() == expected_response


def test_get_post_does_not_exist(server, valid_post, data_filepath):
    write_to_file(data_filepath, valid_post)
    response = requests.get('http://localhost:8087/posts/123')
    assert response.status_code == 404
    assert response.json() == {'message': 'post not found'}


def test_add_new_post(server, valid_post, data_filepath):
    response = requests.post('http://localhost:8087/posts', json=valid_post.__dict__)
    assert response.status_code == 201
    assert response.json() == {'3278c49f7fad11eebf07f46add75dd512': 1}
    assert check_post_exists(data_filepath, valid_post)[0] == True


def test_add_existing_post(server, valid_post, data_filepath):
    write_to_file(data_filepath, valid_post)
    response = requests.post('http://localhost:8087/posts', json=valid_post.__dict__)
    assert response.status_code == 200
    assert response.json() == {'3278c49f7fad11eebf07f46add75dd512': 1}


def test_add_invalid_post(server, valid_post, data_filepath):
    # make post with invalid data
    valid_post.user_post_karma = 'four'
    response = requests.post('http://localhost:8087/posts', json=valid_post.__dict__)
    assert response.status_code == 404
    assert response.json() == {'message': 'invalid post data'}


def test_delete_existing_post(server, valid_post, data_filepath):
    write_to_file(data_filepath, valid_post)
    response = requests.delete('http://localhost:8087/posts/3278c49f7fad11eebf07f46add75dd512')
    assert response.status_code == 200
    assert response.json() == {'message': 'post deleted'}


def test_delete_post_does_not_exist(server, valid_post, data_filepath):
    write_to_file(data_filepath, valid_post)
    response = requests.delete('http://localhost:8087/posts/123')
    assert response.status_code == 404
    assert response.json() == {'message': 'post not found'}


def test_update_post(server, valid_post, data_filepath, updated_post):
    write_to_file(data_filepath, valid_post)
    response = requests.put('http://localhost:8087/posts/3278c49f7fad11eebf07f46add75dd512', json=updated_post.__dict__)
    assert response.status_code == 200
    assert response.json() == {'message': 'post updated'}


def test_update_post_does_not_exist(server, valid_post, data_filepath, post_does_not_exist):
    write_to_file(data_filepath, valid_post)
    response = requests.put('http://localhost:8087/posts/512',
                            json=post_does_not_exist.__dict__)
    assert response.status_code == 404
    assert response.json() == {'message': 'post not found'}


def test_update_invalid_post(server, valid_post, data_filepath):
    write_to_file(data_filepath, valid_post)
    # make post with invalid data
    valid_post.user_post_karma = 'five'
    response = requests.put('http://localhost:8087/posts/3278c49f7fad11eebf07f46add75dd512', json=valid_post.__dict__)
    assert response.status_code == 404
    assert response.json() == {'message': 'invalid post data'}


def test_path_not_registered(server):
    response = requests.get('http://localhost:8087/wrong-path')
    assert response.status_code == 404
    assert response.json() == {'message': 'route not found'}
