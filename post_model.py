from pydantic import BaseModel


class Post(BaseModel):
    UNIQUE_ID: str
    link: str
    username: str
    user_cake_data: str
    user_post_karma: int
    user_comment_karma: int
    post_date: str
    number_of_comments: int
    number_of_votes: int
    post_category: str

    def get_data(self):
        return f'{self.UNIQUE_ID};{self.link};{self.username};{self.user_cake_data};' \
               f'{self.user_post_karma};{self.user_comment_karma};{self.post_date};' \
               f'{self.number_of_comments};{self.number_of_votes};{self.post_category}'


