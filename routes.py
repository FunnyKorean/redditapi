from dataclasses import dataclass
from enum import Enum
from collections import defaultdict
from pydantic import ValidationError

from files import write_to_file, get_file_name, get_records, get_record, delete_record, update_record
from post_model import Post


@dataclass
class Response:
    message: dict
    status: int


class Method(Enum):
    POST = 'POST_ROUTES'
    GET = 'GET_ROUTES'
    PUT = 'PUT_ROUTES'
    DELETE = 'DELETE_ROUTES'


ROUTES = defaultdict(lambda: dict())

templates = []


def route(path, method=Method.GET):
    def decorator(func):
        if path not in templates:
            templates.append(path)

        # Register the path in routes
        path_name = path
        method_routes = ROUTES[method]
        method_routes[path_name] = func

        def wrapper(*args, **kwargs) -> Response:
            resp = func(*args, **kwargs)
            return resp

        return wrapper

    return decorator


@route(path='/posts/')
def get_posts(body: dict, **kwargs) -> Response:
    return Response({'message': get_records(get_file_name())}, 200)


@route(path='/posts/<str:post_id>')
def get_post(body: dict, post_id: str) -> Response:
    post = get_record(get_file_name(), post_id)

    if post:
        return Response(post, 200)
    return Response({'message': 'post not found'}, 404)


@route(path='/posts/', method=Method.POST)
def create_post(data: dict, **kwargs) -> Response:
    try:
        post = Post(**data)

    except ValidationError:
        return Response({'message': 'invalid post data'}, 404)

    line_number, post_exists = write_to_file(get_file_name(), post)

    if not post_exists:
        return Response({data['UNIQUE_ID']: line_number}, 201)
    return Response({data['UNIQUE_ID']: line_number}, 200)


@route(path='/posts/<str:post_id>', method=Method.DELETE)
def delete_post(data: dict, post_id: str) -> Response:
    is_deleted = delete_record(get_file_name(), post_id)

    if not is_deleted:
        return Response({'message': 'post not found'}, 404)

    return Response({'message': 'post deleted'}, 200)


@route(path='/posts/<str:post_id>', method=Method.PUT)
def update_post(data: dict, post_id: str) -> Response:
    try:
        post = Post(**data)

    except ValidationError:
        return Response({'message': 'invalid post data'}, 404)

    is_updated = update_record(get_file_name(), post)
    if not is_updated:
        return Response({'message': 'post not found'}, 404)
    return Response({'message': 'post updated'}, 200)
